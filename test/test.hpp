#include <iostream>

class TClass
{
	void f();
	void d() { std::cout << "hello world" << std::endl; }
	virtual void pureF() = 0;
	TClass() = default;
	TClass(TClass&& other) = delete;
	static void staticPrivate();

	public:
	static void staticMethod();
};

template<class T>
class TTClass
{
	static void staticMethod();
	void f(T);
	void d(T) {}
};

template<class T>
void tFun(T param)
{
}

template<class T>
void tOtherFun(T param);

template<class T>
void tOtherFunRed(T param);

template<class T>
void tOtherFunRed(T param){};

int fun2(float first1, double second2);
int fun(float first1, double second2)
		/*wasdaa*/;
int forwardDec(int first, long second);
int forwardDec(int first, long second);
int forwardDec(int first, long second) {}

void red();
void red();

int a;
int b;
static int d;
int __var;

static void staticFun();

int main() { return 0; }
