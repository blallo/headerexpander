import os
import argparse
import shutil
from tempfile import gettempdir

FAILED_FILES = []
SUCCEDED_FILES = []
FIXED_FILE_LIST = "./HeaderExpander.txt"

LOG_LEVES = ["INFO: ", "", "WARNING: ", "ERROR: "]

def log(message, log_level=1):
    if log_level >= ARGS.quiet_level:
        print(LOG_LEVES[log_level] + message)

def get_expander_command(path_to_file):
    return HEADER_EXPANDER_DIR + "/main -g -f='./' -d=" + FIXED_FILE_LIST + " -i " + path_to_file + " -- -w " + ARGS.compile_flags

def get_compiler_command():
    return "gcc out.c -c -shared -ggdb3 -O1 " + ARGS.compile_flags

def on_fail(full_path):

    FAILED_FILES.append(full_path)

    if not ARGS.no_wait:
        input()

    if ARGS.terminate_after_error:
        exit(-1)

def run_expander(full_path):
    tool_command = get_expander_command(full_path)
    log("Running tool: " + tool_command, 0)
    exit_code = os.system(tool_command)

    if exit_code != 0:
        log("Failed to expand " + full_path, 2)
        log("Exit status: " + str(exit_code))

    return exit_code

def run_compiler(full_path):
    log("Running compiler: " + get_compiler_command(), 0)
    exit_code = os.system(get_compiler_command())

    if exit_code != 0:
        log("Failed to compile " + full_path, 2)
        log("Exit status: " + str(exit_code))

    return exit_code

def move_generated_files(relative_file_name, relative_file_dir):
    object_file_dir = OUTPUT_DIR + "/" + relative_file_dir

    if not os.path.isdir(object_file_dir):
        log("Creating dir " + object_file_dir)
        os.makedirs(object_file_dir)

    destination_file_name = OUTPUT_DIR + "/" + relative_file_name + ".o"
    log("Coping .o file to: " + destination_file_name)
    log("Evaluating destination folder to " + object_file_dir, 0)
    shutil.copyfile("out.o", destination_file_name)
    SUCCEDED_FILES.append(relative_file_name)

def process_file(full_path, relative_file_dir):
    if not (full_path.endswith(".h") or full_path.endswith(".hpp")):
        log("Not a header file, skipping", 0)
        return

    log("Processing " + full_path)

    success = (run_expander(full_path) == 0) and (run_compiler(full_path) == 0)

    if success:
        move_generated_files(full_path, relative_file_dir)
        reset_dir()
    else:
        on_fail(full_path)

def reset_file(file_relative_path):
    file_relative_path = file_relative_path.replace("\n", "")

    file_abs_path = os.path.abspath(file_relative_path)
    original_file_name = file_abs_path[len(LAUNCH_DIR):]

    log("Resetting file " + file_relative_path, 0)
    log("With " + original_file_name, 0)

    shutil.copyfile(COPY_DIR+original_file_name, file_relative_path)

def reset_dir():
    log("Resetting dir", 0)
    if not os.path.isfile(FIXED_FILE_LIST):
        log("Could not reset dir becaused there was no fixed file list")
        return

    modified_files = open(FIXED_FILE_LIST, "r")

    for modified_file in modified_files:
        reset_file(modified_file)

    modified_files.close()
    os.remove(FIXED_FILE_LIST)
    os.remove("./out.o")
    log("Done resetting dir", 0)

def process_dir(dir_path):
    log("entering  " + dir_path)
    for relative_file_name in os.listdir(dir_path):
        full_path = dir_path + "/" + relative_file_name
        if os.path.isdir(full_path):
            process_dir(full_path)
        else:
            process_file(full_path, dir_path)
    log("leaving " + dir_path)

def clone_current_dir():
    if os.path.isdir(COPY_DIR):
        shutil.rmtree(COPY_DIR)

    shutil.copytree(LAUNCH_DIR, COPY_DIR)
    log("Original folder cloned", 0)

def start():
    clone_current_dir()
    process_dir(".")

def report():
    log("Failed Files:")
    for file_name in FAILED_FILES:
        log(file_name)

class ReadableDir(argparse.Action):
    def __call__(self, parser, namespace, values, _=None):
        prospective_dir = values

        if not os.path.isdir(prospective_dir):
            raise argparse.ArgumentTypeError("ReadableDir:{0} is not a valid path".format(prospective_dir))

        if os.access(prospective_dir, os.R_OK):
            setattr(namespace, self.dest, prospective_dir)
        else:
            raise argparse.ArgumentTypeError("ReadableDir:{0} is not a readable dir".format(prospective_dir))

PARSER = argparse.ArgumentParser(description='runner of header expander', fromfile_prefix_chars="@")
PARSER.add_argument('-l', '--launch_directory', action=ReadableDir, default="./", help="location in where this script will be run")
PARSER.add_argument('-e', '--header_expander_location', action=ReadableDir, default="header_expander", help="directory that contains the header expander executable")
PARSER.add_argument('-o', '--output_dir', action=ReadableDir, default="./output/", help="location of the output folder")
PARSER.add_argument('-n', '--no_wait', type=bool, default=False, help='if provided, the program will not wait after an error')
PARSER.add_argument('-t', '--terminate_after_error', type=bool, default=False, help='if provided, the program will exit after the first error')
PARSER.add_argument('-r', '--report', type=bool, default=False, help='provide a report of failed files at the end of the execution (overwritten by -q=2)')
PARSER.add_argument('-f', '--compile_flags', default="", help="extra args to provide to the tool and the compiler")
PARSER.add_argument('-q', '--quiet_level', type=int, default=1, help='0 is verbose, 1 is standard, 2 is quiet. Does not prevent invoked commands to write')

ARGS = PARSER.parse_args()

LAUNCH_DIR = os.path.abspath(ARGS.launch_directory)
HEADER_EXPANDER_DIR = os.path.abspath(ARGS.header_expander_location)
OUTPUT_DIR = os.path.abspath(ARGS.output_dir)
os.chdir(LAUNCH_DIR)
COPY_DIR = gettempdir() + "/" + os.path.basename(os.path.dirname(os.path.realpath(__file__)))

log("Running on: " + LAUNCH_DIR, 0)
log("Header Expander location: " + HEADER_EXPANDER_DIR, 0)
log("Output location: " + OUTPUT_DIR, 0)
log("Selecting support dir " + COPY_DIR, 0)

start()

if ARGS.report:
    report()

log(str(len(FAILED_FILES)) + " compilations failed\n" + str(len(SUCCEDED_FILES)) + " compilations succeded")
