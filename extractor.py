import sys
import re
import argparse

LOG_LEVES = ["INFO: ", "", "WARNING: ", "ERROR: "]

def log(message, log_level=1):
    if log_level >= ARGS.quiet_level:
        print(LOG_LEVES[log_level] + message)

def get_block():

    while True:
        char = INPUT.read(1)
        if not char:
            log("Found end of file")
            return None
        if char == '{':
            log("Found block",0)
            break

    buf = ['{']

    while True:
        char = INPUT.read(1)
        buf.append(char)

        if not char:
            log("End of file inside block", 2)
            return None

        if char == '}':
            break

        if char == '{':
            log("Block is malformed", 2)
            return None

    return "".join(buf)


def evaluate_single_arg(arg):
    result = re.match(r"\s*0x\S*\s*\S*(.*)\s*", arg, re.DOTALL)
    attributes = {}
    evaluate_attributes(result.group(1), attributes)
    return attributes

def evaluate_args(args, ls):
    single_arg = r"\s*<ARGUMENT>\s*(.*?)\s*</ARGUMENT>\s*"
    result = re.match(single_arg+r"((" + single_arg + r")*)", args, re.DOTALL)
    ls.append(evaluate_single_arg(result.group(1)))
    if not empty_or_space_or_none(result.group(2)):
        evaluate_args(result.group(2), ls)

def evaluate_subroutine_attribute(field_name, field_value, out_dict):
    if "DW_AT_name" in field_name:
        result = re.match(r'"(.*)"', field_value)
        out_dict["NAME"] = result.group(1)
        return
    if "DW_AT_type" in field_name:
        result = re.match(r'0x\S*\s*"(.*)"', field_value, re.DOTALL)
        out_dict["TYPE"] = result.group(1)
        return
    if "DW_AT_location" in field_name:
        out_dict["LOCATION"] = field_value
        return
    # out_dict[field_name] = field_value

def empty_or_space_or_none(s):
    return s is None or s.isspace() or s is ""

def evaluate_attributes(attr, out_dict):
    par_content = r'(?P<par>((false)|(true)|(0x\S*?)|(".*?")|([-+]?[0-9]+)|(0x\S*\s*".*?")|(0x\S*\s*(\[.*?\):.*)|(\S*\s*[-+]?[0-9]+))))'
    single_attr = r"\s*(?P<field>\S*)\s*(\(" + par_content + "\))\s*"
    result = re.match(single_attr + r"(?P<others>.*)", attr, re.DOTALL)
    while result is not None:
        attr = result.group('others')
        evaluate_subroutine_attribute(result.group('field'), result.group('par'), out_dict)
        result = re.match(single_attr + r"(?P<others>.*)", attr, re.DOTALL)

def evaluate_subroutine(text):
    result = re.match(r"\s*0x\S*\s*\S*(.*)\s*", text, re.DOTALL)
    dicttionary = {}
    evaluate_attributes(result.group(1), dicttionary)
    return dicttionary
    # print(to_return)

def dump_subroutine(subroutine_info, args_info):
    if subroutine_info.has_key("NAME"):
        print("FUNCTION NAME: "+subroutine_info["NAME"])
    else:
        print ("MISSING FUNCTION NAME")

    if subroutine_info.has_key("TYPE"):
        print("TYPE: " + subroutine_info["TYPE"])

    for arg in args_info:
        if arg.has_key("NAME"):
            print("\tARG NAME: "+arg["NAME"])
        else:
            print("MISSING ARG NAME")

        if arg.has_key("TYPE"):
            print("\tTYPE: " + arg["TYPE"])
        if arg.has_key("LOCATION"):
            print("\tLOCATION "+arg["LOCATION"])

def evaluate_block(block):
    log("Evaluating block " + block, 0)
    result = re.match(r"\s*{(.*)<ARGSLIST>(.*)</ARGSLIST>\s*}", block, re.DOTALL)
    subdict = evaluate_subroutine(result.group(1))
    ls = []
    if not empty_or_space_or_none(result.group(2)):
        evaluate_args(result.group(2), ls)

    dump_subroutine(subdict, ls)

def start():
    block = get_block()
    while block:
        evaluate_block(block)
        block = get_block()

PARSER = argparse.ArgumentParser(description='runner of header expander', fromfile_prefix_chars="@")
PARSER.add_argument('-q', '--quiet_level', type=int, default=1, help='0 is verbose, 1 is standard, 2 is quiet. Does not prevent invoked commands to write')
PARSER.add_argument('-i', '--input_file', type=str, default="", help='input file, if not provided stdin will be used instead')
ARGS = PARSER.parse_args()

INPUT = sys.stdin
if ARGS.input_file is not "":
    INPUT = open(ARGS.input_file)

start()
