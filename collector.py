import os
import argparse
import shutil
from tempfile import gettempdir

LOG_LEVES = ["INFO: ", "", "WARNING: ", "ERROR: "]
def log(message, log_level=1):
    if log_level >= ARGS.quiet_level:
        print(LOG_LEVES[log_level] + message)

def on_fail(full_path):

    if not ARGS.no_wait:
        input()

    if ARGS.terminate_after_error:
        exit(-1)

def get_extractor_command(full_path):
    return EXTRATOR_DIR + "/extractor " + full_path

def run_extractor(full_path):
    tool_command = get_extractor_command(full_path)
    log("Running tool: " + tool_command, 0)
    exit_code = os.system(tool_command)

    if exit_code != 0:
        log("Failed to expand " + full_path, 2)
        log("Exit status: " + str(exit_code))

    return exit_code

def process_file(full_path, relative_file_dir):
    if not full_path.endswith(".o"):
        log("Not a object file, skipping", 0)
        return

    log("Processing " + full_path)
    exit_code = run_extractor(full_path)
    if exit_code != 0:
        on_fail(full_path)

def process_dir(dir_path):
    log("Entering  " + dir_path,0)

    for relative_file_name in os.listdir(dir_path):
        full_path = dir_path + "/" + relative_file_name
        if os.path.isdir(full_path):
            process_dir(full_path)
        else:
            process_file(full_path, dir_path)

    log("Leaving " + dir_path, 0)


class ReadableDir(argparse.Action):
    def __call__(self, parser, namespace, values, _=None):
        prospective_dir = values

        if not os.path.isdir(prospective_dir):
            raise argparse.ArgumentTypeError("ReadableDir:{0} is not a valid path".format(prospective_dir))

        if os.access(prospective_dir, os.R_OK):
            setattr(namespace, self.dest, prospective_dir)
        else:
            raise argparse.ArgumentTypeError("ReadableDir:{0} is not a readable dir".format(prospective_dir))

PARSER = argparse.ArgumentParser(description='Extracts all the parameters name from a .o file.', fromfile_prefix_chars="@")

PARSER.add_argument('-l', '--select-dir', action=ReadableDir, default="./", help="location where .o file will be searched")
PARSER.add_argument('-e', '--extractor-location', action=ReadableDir, default="header_expander", help="directory that contains the extractor executible")
PARSER.add_argument('-t', '--terminate_after_error', type=bool, default=False, help='if provided, the program will exit after the first error')
PARSER.add_argument('-n', '--no_wait', type=bool, default=False, help='if provided, the program will not wait after an error')
PARSER.add_argument('-q', '--quiet_level', type=int, default=1, help='0 is verbose, 1 is standard, 2 is quiet. Does not prevent invoked commands to write')

ARGS = PARSER.parse_args()
LAUNCH_DIR = os.path.abspath(ARGS.select_dir)
EXTRATOR_DIR = os.path.abspath(ARGS.extractor_location)

log("Header Expander location: " + EXTRATOR_DIR, 0)
log("Running on: " + LAUNCH_DIR, 0)

process_dir(LAUNCH_DIR)
