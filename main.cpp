#include <clang/ASTMatchers/ASTMatchFinder.h>
#include <clang/ASTMatchers/ASTMatchers.h>
#include <clang/Basic/Diagnostic.h>
#include <clang/Basic/DiagnosticOptions.h>
#include <clang/Frontend/FrontendActions.h>
#include <clang/Frontend/TextDiagnosticPrinter.h>
#include <clang/Rewrite/Core/Rewriter.h>
#include <clang/Tooling/CommonOptionsParser.h>
#include <clang/Tooling/Refactoring.h>
#include <clang/Tooling/Tooling.h>
#include <cstdlib>
#include <fstream>
#include <llvm/Support/CommandLine.h>

constexpr int pathNameSize = 4096;

using namespace clang;
using namespace tooling;
using namespace llvm;
using namespace ast_matchers;
using PathName = SmallVector<char, pathNameSize>;

static llvm::cl::OptionCategory HeaderExpanderCategory(
		"Header Expander options");
static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);
static cl::extrahelp MoreHelp("\nSomething about this tool \n");
static cl::opt<bool> Inplace(
		"i", cl::desc("Overwrite edited files."), cl::cat(HeaderExpanderCategory));

static cl::opt<bool> genGlobalFile(
		"g", cl::desc("Generate including file"), cl::cat(HeaderExpanderCategory));

static cl::opt<bool> usingCpp(
		"cpp",
		cl::desc("Dump a cpp11 compatible file"),
		cl::cat(HeaderExpanderCategory));

static cl::opt<std::string> execLocation(
		"f",
		cl::desc("Path to to folder that can be modified, ./ by deafult"),
		cl::init("./"),
		cl::cat(HeaderExpanderCategory));

static cl::opt<std::string> globalsTargetFile(
		"o",
		cl::desc("Output file for the globals files"),
		cl::init("./out.c"),
		cl::cat(HeaderExpanderCategory));

static cl::opt<std::string> dumpFiles(
		"d",
		cl::desc("Output file for the modified file list"),
		cl::init("."),
		cl::cat(HeaderExpanderCategory));

PathName targetFolder;

bool startsWith(const char *pre, const char *str)
{
	size_t lenpre = strlen(pre);
	size_t lenstr = strlen(str);
	return lenstr < lenpre ? false : strncmp(pre, str, lenpre) == 0;
}

bool isInPath(const std::string &file)
{
	if (file.empty())
		return false;
	PathName fil;
	sys::fs::real_path(Twine(file), fil, true);
	return startsWith(&targetFolder[0], &fil[0]);
}

// inspired by
// https://github.com/llvm-mirror/clang-tools-extra/blob/master/clang-reorder-fields/ReorderFieldsAction.cpp

class DeclarationPrinter: public MatchFinder::MatchCallback
{
	public:
	DeclarationPrinter(std::map<std::string, Replacements> &replacements)
			: replacements(replacements)
	{
	}

	void run(const MatchFinder::MatchResult &result) override
	{
		if (auto dec = result.Nodes.getNodeAs<FunctionDecl>("Declarations"))
		{
			// we need to check that is not defined later, that is not redeclared
			// later, and that is not a pure cxx function since this cheks cannot be
			// made with the matcher.
			std::string fileName(
					result.SourceManager->getFilename(dec->getLocation()).str());
			if (!dec->isDefined() && !dec->isPure() &&
					dec->getMostRecentDecl() == dec && isInPath(fileName))
				addReplacement(dec->getSourceRange(), *result.Context);
		}

		if (auto dec = result.Nodes.getNodeAs<VarDecl>("GlobalVar"))
		{
			std::string fileName(
					result.SourceManager->getFilename(dec->getLocation()).str());
			if (!dec->isTemplated() && isInPath(fileName))
				globalSymbols.push_back(dec->getQualifiedNameAsString());
		}

		if (auto dec = result.Nodes.getNodeAs<FunctionDecl>("GlobalFunction"))
		{
			std::string fileName(
					result.SourceManager->getFilename(dec->getLocation()).str());
			if (!dec->isTemplated() && isInPath(fileName))
				globalSymbols.push_back(dec->getQualifiedNameAsString());
		}
	}

	void generateGlobalArray(const std::string &mainFileName)
	{
		int a = 0;
		std::ofstream file;
		file.open(globalsTargetFile);
		file << "#include \"" << mainFileName << "\"\n";
		for (auto v : globalSymbols)
		{
			if (!usingCpp)
				file << "void* ";
			else
				file << "auto ";
			file << "headerExpansion" << a << "= ";
			file << "&" << v << ";\n";
			a++;
		}

		file.close();
	}

	void dumpTouchedFileLoc(const std::string &outFile)
	{
		std::ofstream file;
		file.open(outFile);
		for (auto v : modifiedFiles)
			file << v << "\n";

		file.close();
	}

	void addReplacement(SourceRange Old, const ASTContext &Context)
	{
		std::string NewText = Lexer::getSourceText(
				CharSourceRange::getTokenRange(Old),
				Context.getSourceManager(),
				Context.getLangOpts());
		NewText += "{}";
		tooling::Replacement R(
				Context.getSourceManager(),
				CharSourceRange::getTokenRange(Old),
				NewText,
				Context.getLangOpts());

		std::string loc(R.getFilePath());
		modifiedFiles.insert(loc);
		consumeError(replacements[R.getFilePath()].add(R));
	}

	private:
	std::set<std::string> modifiedFiles;
	std::vector<std::string> globalSymbols;
	std::map<std::string, Replacements> &replacements;
};

void report(DeclarationPrinter &printer, RefactoringTool &tool, int exitCode)
{
	if (exitCode != 0)
		return;

	if (genGlobalFile)
		printer.generateGlobalArray(tool.getSourcePaths()[0]);

	if (dumpFiles != ".")
		printer.dumpTouchedFileLoc(dumpFiles);
}

// copied from
// https://github.com/llvm-mirror/clang-tools-extra/blob/master/clang-reorder-fields/tool/ClangReorderFields.cpp
int main(int argc, const char **argv)
{
	CommonOptionsParser OptionParser(argc, argv, HeaderExpanderCategory);
	sys::fs::real_path(execLocation, targetFolder);

	auto Files = OptionParser.getSourcePathList();
	tooling::RefactoringTool tool(OptionParser.getCompilations(), Files);

	DeclarationPrinter printer(tool.getReplacements());
	MatchFinder finder;

	DeclarationMatcher dclMatcher =
			functionDecl(
					allOf(
							unless(isExpansionInSystemHeader()),
							unless(anyOf(isDefinition(), isDefaulted(), isDeleted()))))
					.bind("Declarations");

	DeclarationMatcher staticFunction =
			functionDecl(allOf(
											 isStaticStorageClass(),
											 unless(isExpansionInSystemHeader()),
											 anyOf(unless(cxxMethodDecl()), isPublic())))
					.bind("GlobalFunction");
	DeclarationMatcher staticVars =
			varDecl(allOf(
									hasGlobalStorage(),
									unless(isStaticLocal()),
									unless(isExpansionInSystemHeader()),
									anyOf(unless(cxxMethodDecl()), isPublic())))
					.bind("GlobalVar");

	finder.addMatcher(dclMatcher, &printer);
	finder.addMatcher(staticFunction, &printer);
	finder.addMatcher(staticVars, &printer);
	auto factory = tooling::newFrontendActionFactory(&finder);

	if (Inplace)
	{
		int out = tool.runAndSave(factory.get());

		report(printer, tool, out);

		return out;
	}

	int ExitCode = tool.run(factory.get());
	LangOptions DefaultLangOptions;
	IntrusiveRefCntPtr<DiagnosticOptions> DiagOpts(new DiagnosticOptions());
	TextDiagnosticPrinter DiagnosticPrinter(errs(), &*DiagOpts);
	DiagnosticsEngine Diagnostics(
			IntrusiveRefCntPtr<DiagnosticIDs>(new DiagnosticIDs()),
			&*DiagOpts,
			&DiagnosticPrinter,
			false);

	auto &FileMgr = tool.getFiles();
	SourceManager Sources(Diagnostics, FileMgr);
	Rewriter Rewrite(Sources, DefaultLangOptions);
	tool.applyAllReplacements(Rewrite);

	for (const auto &File : Files)
	{
		const auto *Entry = FileMgr.getFile(File);
		const auto ID = Sources.getOrCreateFileID(Entry, SrcMgr::C_User);
		Rewrite.getEditBuffer(ID).write(outs());
	}

	report(printer, tool, ExitCode);

	return ExitCode;
}
