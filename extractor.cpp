#include "llvm/DebugInfo/DIContext.h"
#include "llvm/DebugInfo/DWARF/DWARFContext.h"
#include "llvm/DebugInfo/DWARF/DWARFExpression.h"
#include "llvm/MC/MCRegisterInfo.h"
#include "llvm/Object/Archive.h"
#include "llvm/Object/MachOUniversal.h"
#include "llvm/Object/ObjectFile.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/InitLLVM.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/ToolOutputFile.h"
#include "llvm/Support/WithColor.h"

using namespace llvm;
using namespace object;
using namespace dwarf;

static cl::OptionCategory ExtractorCategory("Extractor options");
static cl::opt<bool> Help(
		"h", cl::desc("Alias for -help"), cl::Hidden, cl::cat(ExtractorCategory));

static cl::opt<std::string> OutputFilename(
		"o",
		cl::init(""),
		cl::desc("Redirect output to the specified file."),
		cl::value_desc("filename"));

static cl::list<std::string> InputFilenames(
		cl::Positional,
		cl::desc("<input object files>"),
		cl::ZeroOrMore,
		cl::cat(ExtractorCategory));

using HandlerFn = std::function<bool(
		ObjectFile &, DWARFContext &DICtx, Twine, raw_ostream &)>;

struct RegisterOp
{
	public:
	std::string operationEncodingString{ "" };
	std::string registerName{ "" };
	int64_t operand[2]{ 0, 0 };
	bool good;

	RegisterOp(bool isGood = true): good(isGood) {}

	void dump(raw_ostream &OS)
	{
		if (registerName.empty())
		{
			OS << operationEncodingString << " ";
			OS << operand[0] << " ";
			OS << operand[1];
		}
		else
		{
			OS << registerName;
		}
	}
};

struct Location
{
	private:
	RegisterOp operation;
	uint64_t begin{ std::numeric_limits<uint64_t>::min() };
	uint64_t end{ std::numeric_limits<uint64_t>::max() };

	public:
	Location(RegisterOp op): operation(std::move(op)) {}

	Location(RegisterOp op, uint64_t b, uint64_t e)
			: operation(std::move(op)), begin(b), end(e)
	{
	}

	void dump(raw_ostream &OS) { operation.dump(OS); }
};

class LocationList
{
	private:
	std::vector<Location> locations;

	public:
	void addLocation(Location location)
	{
		locations.push_back(std::move(location));
	}
	void dump(raw_ostream &OS)
	{
		std::for_each(
				locations.begin(), locations.end(), [&OS](auto loc) { loc.dump(OS); });
	}
};

class Parameter
{
	private:
	LocationList loc;
	std::string name;

	public:
	Parameter(std::string nm, LocationList ls)
			: name(std::move(nm)), loc(std::move(ls))
	{
	}
	Parameter(std::string nm): name(std::move(nm)) {}
	void dump(raw_ostream &OS)
	{
		OS << name << ", ";
		loc.dump(OS);
	}
};

class Function
{
	private:
	std::vector<Parameter> parameters;
	std::string name;
	uint64_t low_pc;

	public:
	Function(std::string nm, uint64_t low = 0): name(std::move(nm)), low_pc(low)
	{
	}
	void addParameter(Parameter param) { parameters.push_back(std::move(param)); }
	void dump(raw_ostream &OS)
	{
		OS << name;
		// OS << "Low pc " << low_pc << "\n";
		std::for_each(parameters.begin(), parameters.end(), [&OS](auto loc) {
			OS << ", ";
			loc.dump(OS);
		});
		OS << "\n";
	}

	bool lowInBound(uint64_t low, uint64_t high)
	{
		return low_pc >= low && low_pc <= high;
	}
};

static std::string manageRegisterOp(
		uint8_t Opcode, uint64_t Operands[2], const MCRegisterInfo *MRI, bool isEH)
{
	if (MRI == nullptr)
		return "";

	uint64_t DwarfRegNum;
	unsigned OpNum = 0;

	if (Opcode == DW_OP_bregx || Opcode == DW_OP_regx)
		DwarfRegNum = Operands[OpNum++];
	else if (Opcode >= DW_OP_breg0 && Opcode < DW_OP_bregx)
		DwarfRegNum = Opcode - DW_OP_breg0;
	else
		DwarfRegNum = Opcode - DW_OP_reg0;

	int LLVMRegNum = MRI->getLLVMRegNum(DwarfRegNum, isEH);
	if (LLVMRegNum < 0)
		return "";

	if (const char *RegName = MRI->getName(LLVMRegNum))
	{
		if ((Opcode >= DW_OP_breg0 && Opcode <= DW_OP_breg31) ||
				Opcode == DW_OP_bregx)
		{
			std::string to_write;
			llvm::raw_string_ostream ostream(to_write);
			ostream << format("%s%+" PRId64, RegName, Operands[OpNum]) << "\n";
			ostream.flush();
			return ostream.str();
		}

		return std::string(RegName);
	}
	return "";
}

RegisterOp manageOperation(
		DWARFExpression::Operation &op, const MCRegisterInfo *RegInfo, bool isEH)
{
	RegisterOp toReturn;
	if (op.isError())
	{
		toReturn.good = false;
		return toReturn;
	}
	uint8_t Opcode = op.getCode();

	toReturn.operationEncodingString = OperationEncodingString(Opcode);
	assert(!toReturn.operationEncodingString.empty() && "DW_OP has no name!");
	uint64_t Operands[2] = { op.getRawOperand(0), op.getRawOperand(1) };

	if ((Opcode >= DW_OP_breg0 && Opcode <= DW_OP_breg31) ||
			(Opcode >= DW_OP_reg0 && Opcode <= DW_OP_reg31) ||
			Opcode == DW_OP_bregx || Opcode == DW_OP_regx)
	{
		toReturn.registerName = manageRegisterOp(Opcode, Operands, RegInfo, isEH);
		if (!toReturn.registerName.empty())
			return toReturn;
	}

	for (unsigned Operand = 0; Operand < 2; ++Operand)
	{
		unsigned Size = op.getDescription().Op[Operand];
		unsigned Signed = Size & DWARFExpression::Operation::SignBit;

		if (Size == DWARFExpression::Operation::SizeNA)
			break;

		if (Size == DWARFExpression::Operation::SizeBlock)
		{
			WithColor::error() << "Unsupported Size Block\n";
			toReturn.good = false;
			return toReturn;
		}

		if (Signed)
		{
			u_int64_t val = op.getRawOperand(Operand);
			toReturn.operand[Operand] = *reinterpret_cast<int64_t *>(&val);
		}
		else
			toReturn.operand[Operand] = op.getRawOperand(Operand);
	}
	return toReturn;
}

static void error(StringRef Prefix, std::error_code EC)
{
	if (!EC)
		return;
	WithColor::error() << Prefix << ": " << EC.message() << "\n";
	exit(1);
}

RegisterOp manageExp(
		DWARFExpression &exp, const MCRegisterInfo *RegInfo, bool IsEH)

{
	if (std::distance(exp.begin(), exp.end()) != 1)
	{
		WithColor::error()
				<< "the expression was larger than one operation, it will be skipped. "
					 "Operations Count: "
				<< std::distance(exp.begin(), exp.end()) << "\n";
		return RegisterOp(false);
	}

	auto &Op = *exp.begin();
	RegisterOp decodedOp = manageOperation(Op, RegInfo, IsEH);
	return decodedOp;
}

LocationList manageLocation(
		const DWARFFormValue &FormValue, DWARFUnit *U, Function fun)
{
	DWARFContext &Ctx = U->getContext();
	const DWARFObject &Obj = Ctx.getDWARFObj();
	const auto AddressSize = Obj.getAddressSize();
	const MCRegisterInfo *MRI = Ctx.getRegisterInfo();
	LocationList list;
	if (FormValue.isFormClass(DWARFFormValue::FC_Exprloc))
	{
		ArrayRef<uint8_t> Expr = *FormValue.getAsBlock();
		DataExtractor Data(
				StringRef((const char *) Expr.data(), Expr.size()),
				Ctx.isLittleEndian(),
				0);
		DWARFExpression exp(Data, U->getVersion(), U->getAddressByteSize());
		RegisterOp op = manageExp(exp, MRI, false);
		if (op.good)
			list.addLocation(Location(op));
		return list;
	}

	if (FormValue.isFormClass(DWARFFormValue::FC_SectionOffset))
	{
		uint32_t Offset = *FormValue.getAsSectionOffset();
		if (!U->isDWOUnit() && !U->getLocSection()->Data.empty())
		{
			DWARFDebugLoc DebugLoc;
			DWARFDataExtractor Data(
					Obj, *U->getLocSection(), Ctx.isLittleEndian(), Obj.getAddressSize());
			Optional<llvm::DWARFDebugLoc::LocationList> LL =
					DebugLoc.parseOneLocationList(Data, &Offset);
			if (LL)
			{
				uint64_t BaseAddr = 0;
				if (Optional<SectionedAddress> BA = U->getBaseAddress())
					BaseAddr = BA->Address;
				for (const DWARFDebugLoc::Entry &E : LL.getPointer()->Entries)
				{
					DWARFDataExtractor Extractor(
							StringRef(E.Loc.data(), E.Loc.size()),
							Ctx.isLittleEndian(),
							AddressSize);
					DWARFExpression exp(Extractor, dwarf::DWARF_VERSION, AddressSize);
					uint64_t begin = BaseAddr + E.Begin;
					uint64_t end = BaseAddr + E.End;

					RegisterOp op = manageExp(exp, MRI, false);
					if (op.good && fun.lowInBound(begin, end))
						list.addLocation(Location(op, begin, end));
				}
			}
			else
				WithColor::error() << "error extracting location list.";
			return list;
		}

		bool UseLocLists = !U->isDWOUnit();
		StringRef LoclistsSectionData =
				UseLocLists ? Obj.getLoclistsSection().Data : U->getLocSectionData();

		if (!LoclistsSectionData.empty())
		{
			DataExtractor Data(
					LoclistsSectionData, Ctx.isLittleEndian(), Obj.getAddressSize());

			// Old-style location list were used in DWARF v4 (.debug_loc.dwo
			// section). Modern locations list (.debug_loclists) are used starting
			// from v5. Ideally we should take the version from the .debug_loclists
			// section header, but using CU's version for simplicity.
			Optional<llvm::DWARFDebugLoclists::LocationList> LL =
					DWARFDebugLoclists::parseOneLocationList(
							Data, &Offset, UseLocLists ? U->getVersion() : 4);

			uint64_t BaseAddr = 0;
			if (Optional<SectionedAddress> BA = U->getBaseAddress())
				BaseAddr = BA->Address;

			if (LL)
			{
				for (const DWARFDebugLoclists::Entry &E : LL.getPointer()->Entries)
				{
					DWARFDataExtractor Extractor(
							StringRef(E.Loc.data(), E.Loc.size()),
							Ctx.isLittleEndian(),
							Obj.getAddressSize());
					DWARFExpression exp(
							Extractor, dwarf::DWARF_VERSION, Obj.getAddressSize());
					uint64_t begin;
					uint64_t end;
					switch (E.Kind)
					{
						case dwarf::DW_LLE_startx_length:
							begin = E.Value0;
							end = E.Value0 + E.Value1;
							break;
						case dwarf::DW_LLE_start_length:
							begin = E.Value0;
							end = E.Value0 + E.Value1;
							break;
						case dwarf::DW_LLE_offset_pair:
							begin = BaseAddr + E.Value0;
							end = BaseAddr + E.Value1;
							break;
						case dwarf::DW_LLE_base_address:
							BaseAddr = E.Value0;
							break;
						default:
							llvm_unreachable("unreachable locations list kind");
					}

					RegisterOp op = manageExp(exp, MRI, false);
					if (op.good && fun.lowInBound(begin, end))
						list.addLocation(Location(op, begin, end));
				}
			}
			else
				WithColor::error() << "error extracting location list.";
		}
	}
	return list;
}

Parameter manageParameter(DWARFDie &Die, DWARFDie parameter, Function fun)
{
	std::string parameterName;
	if (parameter.getName(DINameKind::ShortName) != nullptr)
		parameterName = parameter.getName(DINameKind::ShortName);

	for (auto &attr : parameter.attributes())
		if (attr.Attr == dwarf::DW_AT_location && attr.isValid())
			return Parameter(
					parameterName, manageLocation(attr.Value, Die.getDwarfUnit(), fun));

	return Parameter(parameterName);
}

void manageSubroutine(DWARFDie Die, raw_ostream &OS)
{
	std::string routineName;

	if (Die.getSubroutineName(DINameKind::ShortName) != nullptr)
		routineName = Die.getSubroutineName(DINameKind::ShortName);

	uint64_t high, low, index;
	Die.getLowAndHighPC(low, high, index);

	Function fun(routineName, low);
	for (auto child : Die.children())
		if (child.getTag() == dwarf::Tag::DW_TAG_formal_parameter)
			fun.addParameter(manageParameter(Die, child, fun));

	fun.dump(OS);
}

void filter(DWARFContext::unit_iterator_range CUs, raw_ostream &OS)
{
	for (const auto &CU : CUs)
	{
		for (const auto &Entry : CU->dies())
		{
			DWARFDie Die = { CU.get(), &Entry };
			if (Die.isSubprogramDIE())
				manageSubroutine(Die, OS);
		}
	}
}

bool dumpObjectFile(
		ObjectFile &Obj, DWARFContext &DICtx, Twine Filename, raw_ostream &OS)
{
	logAllUnhandledErrors(
			DICtx.loadRegisterInfo(Obj), errs(), Filename.str() + ": ");

	filter(DICtx.normal_units(), OS);
	return true;
}

static bool handleBuffer(
		StringRef Filename,
		MemoryBufferRef Buffer,
		HandlerFn HandleObj,
		raw_ostream &OS)
{
	Expected<std::unique_ptr<Binary>> BinOrErr = object::createBinary(Buffer);
	error(Filename, errorToErrorCode(BinOrErr.takeError()));

	bool Result = true;
	if (auto *Obj = dyn_cast<ObjectFile>(BinOrErr->get()))
	{
		std::unique_ptr<DWARFContext> DICtx = DWARFContext::create(*Obj);
		Result = HandleObj(*Obj, *DICtx, Filename, OS);
	}
	return Result;
}

bool handleFile(StringRef Filename, HandlerFn HandleObj, raw_ostream &OS)
{
	ErrorOr<std::unique_ptr<MemoryBuffer>> BuffOrErr =
			MemoryBuffer::getFileOrSTDIN(Filename);
	error(Filename, BuffOrErr.getError());
	std::unique_ptr<MemoryBuffer> Buffer = std::move(BuffOrErr.get());
	return handleBuffer(Filename, *Buffer, HandleObj, OS);
}

int main(int argc, char **argv)
{
	InitLLVM X(argc, argv);
	InitializeAllTargetInfos();
	InitializeAllTargetMCs();

	cl::HideUnrelatedOptions({ ExtractorCategory });
	cl::ParseCommandLineOptions(
			argc, argv, "Extract methods signature from object files");
	if (Help)
	{
		cl::PrintHelpMessage(false, true);
		return 0;
	}

	std::unique_ptr<ToolOutputFile> OutputFile;
	if (!OutputFilename.empty())
	{
		std::error_code EC;
		OutputFile =
				llvm::make_unique<ToolOutputFile>(OutputFilename, EC, sys::fs::F_None);
		error("Unable to open output file" + OutputFilename, EC);
		// Don't remove output file if we exit with an error.
		OutputFile->keep();
	}
	raw_ostream &OS = OutputFile ? OutputFile->os() : outs();
	bool OffsetRequested = false;

	if (InputFilenames.empty())
		InputFilenames.push_back("a.out");

	for (auto Object : InputFilenames)
		handleFile(Object, dumpObjectFile, OS);

	return EXIT_SUCCESS;
}
